document.getElementById("buttonReadProduct").addEventListener("click", readProducts);
document.getElementById("formCreateProduct").addEventListener("submit", createProduct);

function readProducts() {
  var xhr = new XMLHttpRequest();
  xhr.open("GET", "http://localhost:3000/api/product/read.php", true);

  xhr.onload = function() {
    if (this.status == 200) {
      var products = JSON.parse(this.responseText).records;

      var output = "";
      for (var i in products) {
        output +=
          `<div class="product">
            <ul>
              <li>ID:  
                ${products[i].id} 
              </li> 
              <li>Name: 
                ${products[i].name}
              </li>
              <li>Description: 
                ${products[i].description}
              </li>
              <li>price: 
                ${products[i].price}
              </li>
              <li>Category: 
                ${products[i].category_id}, ${products[i].category_name}
              </li>
            </ul>
            </div>`;
      }

      document.getElementById("products").innerHTML = output;
    }
  };

  xhr.send();
}

function createProduct() {
  // e.preventDefault();
  var name = document.getElementById('nameProd').value;
  var price = document.getElementById('priceProd').value;
  var description = document.getElementById('descriptionProd').value;
  var category_id = document.getElementById('category_idProd').value;

  var product = {
    name: name, 
    price: price,
    description: description,
    category_id: category_id,
  };
  
  var xhr = new XMLHttpRequest();

  xhr.open("POST", "http://localhost:3000/api/product/create.php", true);
  // xhr.setRequestHeader("Access-Control-Allow-Origin", "*");

  xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

  xhr.onload = function() {
    var response = JSON.parse(this.responseText);
    var output = "";
    var response_class = "";
    if (this.status == 200) {
      response_class = "created";
    }
    else response_class = "failed_create";
    output +=
      `<div class="${response_class}">
        <p> 
          ${response.message}
        </p>
      </div>`;
    document.getElementById("responseFormCreateProd").innerHTML = output;
  };

  xhr.send(JSON.stringify(product));
}
