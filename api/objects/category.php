<?php
class Category{
 
    // database connection and table name
    private $conn;
    private $table_name = "categories";
 
    // object properties
    public $id;
    public $name;
    public $description;
 
    public function __construct($db){
        $this->conn = $db;
    }
 
    // used by select drop-down list
  public function readAll(){
        //select all data
        $query = "SELECT id, name, description
                  FROM $this->table_name
                  ORDER BY name";
 
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
 
        return $stmt;
  }

  // used by select drop-down list
  public function read(){
  
    //select all data
    $query = "SELECT id, name, description
              FROM $this->table_name
              ORDER BY
                name";

    $stmt = $this->conn->prepare($query);
    $stmt->execute();

    return $stmt;
  }


  // used when filling up the update product form
  function readOne(){
  
    // query to read single record
    $query = "SELECT p.id, p.name, p.description
              FROM $this->table_name p
              WHERE p.id = ?
              LIMIT 0,1";

    // prepare query statement
    $stmt = $this->conn->prepare( $query );

    // bind id of product to be updated
    $stmt->bindParam(1, $this->id);

    // execute query
    $stmt->execute();

    // get retrieved row
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    // set values to object properties
    $this->name = $row['name'];
    $this->description = $row['description'];
  }

  // create category
  function create(){

    // sanitize
    $this->name=htmlspecialchars(strip_tags($this->name));
    $this->description=htmlspecialchars(strip_tags($this->description));
    
    // query to insert record
    $query = "INSERT INTO $this->table_name SET
                name=:name, 
                description=:description,
                created=NOW()
              ";

    // prepare query
    $stmt = $this->conn->prepare($query);
    
    // bind values
    $stmt->bindParam(":name", $this->name);
    $stmt->bindParam(":description", $this->description);
 
    // execute query
    $resposta = $stmt->execute();

    if($resposta) return true;
    else          return false;
  }

  // update the category
  function update(){
  
    // query to read single record
    $query = "SELECT p.id, p.name
              FROM $this->table_name p
              WHERE p.id = ?
              LIMIT 0,1";

    // prepare query statement
    $stmt = $this->conn->prepare($query);

    // bind id of category to be updated
    $stmt->bindParam(1, $this->id);

    // execute query
    $stmt->execute();

    // get retrieved row
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    if($row==false){
      return 202;
    }
    // update query
    $query = "UPDATE $this->table_name SET
                name = :name,
                description = :description,
                modified = NOW()
            WHERE
                id = :id";

    // prepare query statement
    $stmt = $this->conn->prepare($query);

    // sanitize
    $this->name=htmlspecialchars(strip_tags($this->name));
    $this->description=htmlspecialchars(strip_tags($this->description));

    $this->id=htmlspecialchars(strip_tags($this->id));

    // bind new values
    $stmt->bindParam(':name', $this->name);
    $stmt->bindParam(':description', $this->description);

    $stmt->bindParam(':id', $this->id);

    // execute the query
    if($stmt->execute()){
        return 200;
    }

    return 503;
  }

  function delete(){

    // query to read single record
    $query = "SELECT p.id, p.name
              FROM $this->table_name p
              WHERE p.id = ?
              LIMIT 0,1";

    // prepare query statement
    $stmt = $this->conn->prepare($query);

    // bind id of product to be updated
    $stmt->bindParam(1, $this->id);

    // execute query
    $stmt->execute();

    // get retrieved row
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    if($row==false){
      return 202;
    }
    
    // delete query
    $query = "DELETE FROM $this->table_name WHERE id = ?";
 
    // prepare query
    $stmt = $this->conn->prepare($query);
 
    // sanitize
    $this->id=htmlspecialchars(strip_tags($this->id));
 
    // bind id of record to delete
    $stmt->bindParam(1, $this->id);
 
    // execute query
    if($stmt->execute()){
        return 200;
    }
 
    return 503;
     
  }

  // search products
  function search($keywords){
  
    // select all query
    $query = "SELECT
                p.id, p.name, p.description, p.created
            FROM $this->table_name p
            WHERE
                p.name LIKE ? OR p.description LIKE ?
            ORDER BY
                p.created DESC";

    // prepare query statement
    $stmt = $this->conn->prepare($query);

    // sanitize
    $keywords=htmlspecialchars(strip_tags($keywords));
    $keywords = "%{$keywords}%";

    // bind
    $stmt->bindParam(1, $keywords);
    $stmt->bindParam(2, $keywords);

    // execute query
    $stmt->execute();

    return $stmt;
  }

  // read products with pagination
  public function readPaging($from_record_num, $records_per_page){
  
    // select query
    $query = "SELECT
                p.id, p.name, p.description, p.created
            FROM $this->table_name p
            ORDER BY p.created DESC
            LIMIT ?, ?";

    // prepare query statement
    $stmt = $this->conn->prepare( $query );

    // bind variable values
    $stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
    $stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);

    // execute query
    $stmt->execute();

    // return values from database
    return $stmt;
  }

  // used for paging products
  public function count(){
    $query = "SELECT COUNT(*) as total_rows FROM $this->table_name ";

    $stmt = $this->conn->prepare( $query );
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    return $row['total_rows'];
  }
}
?>